
package com.example.freddy.practica3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class actividad3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad3);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return  true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                intent = new Intent(actividad3.this, actividadLogin.class);
                startActivity(intent);
                break;
            case R.id.opcionRegistrar:
                intent = new Intent(actividad3.this, actividadRegistrar.class);
                startActivity(intent);
                break;
            case R.id.opcionAcelerometro:
                intent = new Intent(actividad3.this, actividad_Sensor.class);
                startActivity(intent);
                break;
            case R.id.opcionLuz:
                intent = new Intent(actividad3.this, actividadSensorLuz.class);
                startActivity(intent);
                break;

        }
        return false;
    }



    }

